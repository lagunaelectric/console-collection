﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Compiler Warnings", "RECS0081:Validate Xml docs", Justification = "<Pending>", Scope = "namespace", Target = "~N:ReadMe")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "<Pending>", Scope = "member", Target = "~F:ReadMe.ReadMe.myLongRelevantDescriptiveField")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "<Pending>", Scope = "member", Target = "~F:ReadMe.ReadMe.isTrue")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Redundancies in Symbol Declarations", "RECS0096:Type parameter is never used", Justification = "<Pending>", Scope = "type", Target = "~T:ReadMe.ReadMe.MyNestedClass.MyOtherShit.MyDeepShit`1")]