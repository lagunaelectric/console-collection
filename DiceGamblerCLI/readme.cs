﻿/// <summary>
/// Go to the definition of this namespace and read me!!!
/// </summary>
namespace ReadMe
{
    /// <summary>
    /// Go to the definition of this class and read me!!!
    /// </summary>
    sealed class ReadMe
    {
        // ReMarker is currently broken, please ignore this comment until further notice.
        //
        // You need ReMarker extension for Visual Studio to see this file properly formatted.
        // The read-me is a C# file so we can give code examples throughout and
        // ReMarker will format the comments :)

        #region Extensions 

        //+++                         Extensions
        //?---                    *********************************************
        //
        //++   We will be using the following VS extensions:

        #region Required

        //!+                              Required
        //
        //?                     DO NOT USE REMARKER, IT'S BROKEN FOR MSVS 2017 || It will be re-added as soon as it is working again.
        //?                     C# Essentials
        //?                     CodeMaid
        //?                     Productivity Power Tools 2018/2019
        //?                     Rebracer
        //?                     Refactoring Essentials for Visual Studio
        //?                     Visual Studio Spell Checker

        #endregion Required

        #region Optional

        //!+                              Optional
        //
        //?                     Developer Assistant
        //?                     GhostDoc
        //?                     Indent Guides
        //?                     MultiEditing
        //?                     Regular Expression Explorer
        //!                     Snippet Designer (one of my personal favorites!)
        //?                     Stay Frosty
        //?                     Trailing Whitespace Visualizer
        //?                     WiX Toolset

        #endregion Optional

        #endregion Extensions

        #region Coding Guidelines

        //+++                   Coding Guidelines
        //?---                ############################################
        //++                           House Rules

        #region Commenting

        //!++   Commenting:
        //
        //          We use ReMarker to have commented code. An example file called
        //          ReMarkerCommentDemo.cs will give you a good look at the things
        //          it can do, as well as this read-me, of course.

        #region Fields & Properties

        //?+    Fields & Properties:
        //
        //          You have three options here:
        //              1.) Long, relevant, descriptive name without documentation.
        //              3.) Long, relevant, descriptive name with documentation.
        //              2.) Short, relevant name with descriptive documentation.
        //
        //          All of these are great. After that, it's all about personal preference.
        //          As long as we can figure out why it exists and what it represents easily
        //          you should be good to go
        //
        // Here are a few examples of good declarations:

        public string MyLongRelevantDescriptiveProperty
            = "Something directly to do with the property name.";

        /// <summary>
        /// Description of the field.
        /// </summary>
        int myLongRelevantDescriptiveField = 3;

        bool isTrue = true;

        MyClass B8g4jiTz84J = new MyClass();
        //!+++ ^^^^ DONT FUCKING DO THIS!!!!

        #endregion Fields & Properties

        #region Classes & Methods

        //?+    Classes & Methods:
        //
        //          Classes/methods should ALWAYS have a summary. Even if the purpose
        //          of the class/method is obvious.
        //!         What is obvious to you is not always obvious to others!!

        /// <summary>
        /// My usable object.
        /// </summary>
        class MyClass
        {
            /// <summary>
            /// My functional method.
            /// </summary>
            void MyMethod()
            {
                //- TODO: Stuff.
            }
        }

        #endregion Classes & Methods

        #endregion Commenting

        #region Indentation
        // Run this fucking script in your brain.
        // --------------------------------------
        // TabWidth = 4
        // ReplaceWithSpaces = true
        // BreakUpParametersIfManyExist = true
        // LineUpParameters = true
        // BreakLongStatementsAt = [ dot, operator]
        // BreakAllOrNothing = true
        // LineUpDots = true
        // MaxLineLength = 120
        // --------------------------------------

        /// <summary>
        /// Collapse this, it's used to help illustrate the next example.
        /// </summary>
        class MyNestedClass
        {
            public class MyObject
            {
                public void MyObjectMethod(
                    string a,
                    int b,
                    bool c,
                    long d,
                    double e)
                {
                    //- Todo: Shit
                }
            }

            public class MyObj2
            {
                public class MyChildClass : MyObject
                {
                    //- Todo: Shit.
                }
            }

            public class MyOtherShit
            {
                public class MyDeepShit<T>
                {
                    public int MyMethod(double e, int b) => (int)e * b;
                }
            }
        }

        void LiningUpParameters(
            string a,
            int b,
            bool c,
            long d,
            double e)
        {
            var jay = new MyNestedClass
                          .MyOtherShit
                          .MyDeepShit<
                              MyNestedClass
                             .MyObj2
                             .MyChildClass>();

            var myShit = new MyNestedClass.MyObject();

            myShit.MyObjectMethod(
                a,
                jay.MyMethod(e, b),
                c,
                d,
                e
            );
        }

        #endregion Indentation

        #endregion Coding Guidelines
    }
}