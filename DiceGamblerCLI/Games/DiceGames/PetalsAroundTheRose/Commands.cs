﻿using ConsoleGameHelper;
using System;
using System.Collections.Generic;
using static System.Console;

namespace LagunaConsoleCollection.Games.DiceGames.PetalsAroundTheRose
{
    class Commands : CommandProcessor
    {
        static TextHelper th = new TextHelper();

        public void Answer(int answer)
        {
            var result = Game.RollResult;
            var dice = Game.TheDice;
            Game.RollResult = null;

            if (answer == dice.Answer())
            {
                WriteLine($"{th.n + th.n}Correct!{th.n}");
            }
            else
            {
                WriteLine($"{th.n + th.n}Incorrect...  The correct answer was {result.Answer}{th.n}");
            }
            result.UserAnswer = answer;
            Game.RollResults.Add(result);
            Write("Press the any key...");
            ReadKey();
        }

        public void ShowList(int range)
        {
            Clear();
            List<RollResult> results = Game.RollResults;
            if (range >= results.ToArray().Length)
            {
                range = results.Count;
            }
            for (int i = range - 1; i >= results.Count - range; i--)
            {
                WriteLine(results[i].ToString(true));
            }
            WriteLine(th.n + th.n);
            Write(th.PRESS_CONTINUE);
            ReadKey();
        }
    }
}