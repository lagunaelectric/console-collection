﻿using ConsoleGameHelper;
using System;
using System.Collections.Generic;
using static System.Console;

namespace LagunaConsoleCollection.Games.DiceGames.PetalsAroundTheRose
{
    static class Game
    {
        static readonly string CONTINUE = "Press the any key...";
        static readonly string n = Environment.NewLine;

        static readonly string INTRO_TEXT = "Welcome to Pedals of the Rose!"
            + n + "This game had Bill Gates furious, or so I hear :)";

        static readonly string DaRules = "There are only a few rules:"
            + n + "1.) The name of the game is Petals Around the Rose, and the name is important."
            + n + "2.) Every answer is zero or an even number."
            + n + "3.) There is one correct answer for every throw of the dice."
            + n
            + n + "That's it! You become a Potentate of the Rose when you can correctly"
            + n + "guess the answer 10 times in a row.!!";

        static readonly string commandList = "Commands are typed in the format "
            + "\"<command> <param1> <param2>... etc."
            + n + "> Command | Function(parameters):"
            + n + "> answer     | Answer(int answer): Guess the answer to the roll."
            + n + "> showlist   | ShowList(int range) Shows the last <range> rolls and their answers.";

        public static FiveDice TheDice { get; set; } = new FiveDice();

        public static List<RollResult> RollResults
        {
            get
            {
                return rollResults;
            }

            set
            {
                rollResults = value;
            }
        }

        public static RollResult RollResult
        {
            get { return rollResult; }
            set => rollResult = value;
        }

        private static RollResult rollResult;

        static List<RollResult> rollResults = new List<RollResult>();
        static bool isRunning;
        private static Commands commands = new Commands();

        #region Dice Graphics

        static string[] One = { "     ",
                                "  O  ",
                                "     " };

        static string[] Two = { "O    ",
                                "     ",
                                "    O" };

        static string[] Three = { "O    ",
                                  "  O  ",
                                  "    O" };

        static string[] Four = { "O   O",
                                 "     ",
                                 "O   O" };

        static string[] Five = { "O   O",
                                 "  O  ",
                                 "O   O" };

        static string[] Six = { "O   O",
                                "O   O",
                                "O   O" };

        #endregion Dice Graphics

        public static void Start()
        {
            Clear();
            Title = "Petals Around the Rose";
            WriteLine(INTRO_TEXT);
            WriteLine(n);
            Write(CONTINUE);
            ReadKey();
            Clear();
            WriteLine(DaRules);
            WriteLine(n);
            Write(CONTINUE);
            ReadKey();
            Clear();
            Run();
        }

        static void Run()
        {
            isRunning = true;
            while (isRunning)
            {
                Clear();
                TheDice.Roll();
                var rollResult = new RollResult(TheDice, TheDice.Answer());
                DrawDice(rollResult);
                Write(commandList + n + n + "> ");
                commands.ProcessCommand(ReadLine());
                RollResults.Add(rollResult);
            }
        }

        static void DrawDice(RollResult aRollResult)
        {
            string[][] diceGraphics = { One, Two, Three, Four, Five };
            for (int i = 0; i < aRollResult.Faces.Length; i++)
            {
                switch (aRollResult.Faces[i])
                {
                    case 1:
                        diceGraphics[i] = One;
                        break;

                    case 2:
                        diceGraphics[i] = Two;
                        break;

                    case 3:
                        diceGraphics[i] = Three;
                        break;

                    case 4:
                        diceGraphics[i] = Four;
                        break;

                    case 5:
                        diceGraphics[i] = Five;
                        break;

                    case 6:
                        diceGraphics[i] = Six;
                        break;
                }
            }
            for (int j = 0; j <= 2; j++)
            {
                for (int k = 0; k < diceGraphics.Length; k++)
                {
                    Write(" ");
                    ForegroundColor = ConsoleColor.Black;
                    BackgroundColor = ConsoleColor.White;
                    Write(diceGraphics[k][j]);
                    ResetColor();
                    Write("  ");
                }
                Write(n);
            }
            //WriteLine();
            //WriteLine("Answer: " + rollResult.Answer);
            //WriteLine();
        }

        /// <summary>
        /// Checks if a number is even or not.
        /// </summary>
        /// <param name="number">The number to check.</param>
        /// <returns>True if the number is even, false if it's not.</returns>
        public static bool IsEven(int number) => (number % 2 == 0);
    }
}