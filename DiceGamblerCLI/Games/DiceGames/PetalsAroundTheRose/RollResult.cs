﻿using ConsoleGameHelper;
using System;

namespace LagunaConsoleCollection.Games.DiceGames.PetalsAroundTheRose
{
    class RollResult
    {
        /// <summary>
        /// The dice faces for the outcome.
        /// </summary>
        public int[] Faces { get; set; } = { 1, 1, 1, 1, 1 };

        /// <summary>
        /// The answer of the outcome.
        /// </summary>
        public int Answer { get; set; }

        public int? UserAnswer { get; set; } = null;

        public RollResult(Dice[] dice, int answer)
        {
            for (var i = 0; i <= dice.Length - 1; i++)
            {
                Faces[i] = dice[i];
            }
            Answer = answer;
        }

        public string ToString(bool withAnswer)
        {
            var Out = "";
            for (var i = 0; i < Faces.Length; i++)
            {
                Out += (i == Faces.Length - 1) ? Faces[i].ToString() : Faces[i].ToString() + ", ";
            }
            if (withAnswer)
            {
                Out += $" | Answer:{Answer} ";
                Out += (UserAnswer != null) ? $"Your Answer:{UserAnswer}" : $"Your Answer: Incorrect/Null";
            }
            return Out;
        }

        public static implicit operator string(RollResult x) => x.ToString(true);
    }
}