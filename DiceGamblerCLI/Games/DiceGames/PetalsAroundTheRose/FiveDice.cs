﻿using ConsoleGameHelper;
using System;

namespace LagunaConsoleCollection.Games.DiceGames.PetalsAroundTheRose
{
    class FiveDice
    {
        public Dice[] SetOfDice { get; set; } =
            {
                new Dice(),
                new Dice(),
                new Dice(),
                new Dice(),
                new Dice()
            };

        Random random = new Random();

        /// <summary>
        /// The answer of the current roll.
        /// </summary>
        public int Answer()
        {
            var answer = 0;
            foreach (Dice dice in SetOfDice)
            {
                answer += dice.RosePetals;
            }

            return answer;
        }

        /// <summary>
        /// Rolls the dice.
        /// </summary>
        /// <returns>The outcome of the roll.</returns>
        public void Roll()
        {
            foreach (Dice dice in SetOfDice)
            {
                dice.FaceValue = random.Next(1, 7);
            }
        }

        public static implicit operator Dice[] (FiveDice dice) => dice.SetOfDice;
    }
}