﻿using ConsoleGameHelper;
using System;

namespace LagunaConsoleCollection.Games.DiceGames.TwoDiceGambler
{
    /// <summary>
    /// A pair of Dice.
    /// </summary>
    class PairOfDice
    {
        /// <summary>
        /// The first of the two Dice.
        /// </summary>
        public Dice First { get; set; } = new Dice();

        /// <summary>
        /// The second of the two Dice.
        /// </summary>
        public Dice Second { get; set; } = new Dice();

        readonly Random random = new Random();

        /// <summary>
        /// Adds the face values together.
        /// </summary>
        /// <returns>The sum of the face values of the Dice.</returns>
        public int Total => First.FaceValue + Second.FaceValue;

        /// <summary>
        /// Checks if a number is even or not.
        /// </summary>
        /// <param name="number">The number to check.</param>
        /// <returns>True if the number is even, false if it's not.</returns>
        public bool IsEven(int number) => (number % 2 == 0);

        /// <summary>
        /// Rolls the dice.
        /// </summary>
        public void Roll()
        {
            First.FaceValue = random.Next(1, 7);
            Second.FaceValue = random.Next(1, 7);
        }
    }
}