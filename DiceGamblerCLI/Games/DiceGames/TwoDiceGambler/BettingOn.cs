﻿namespace LagunaConsoleCollection.Games.DiceGames.TwoDiceGambler
{
    enum BettingOn
    {
        Even,
        Odd,
        Doubles
    }
}