﻿namespace LagunaConsoleCollection.Games.DiceGames.TwoDiceGambler
{
    class Bet
    {
        public int Winnings => Amount * 2;

        public int Amount { get; }
        public BettingOn? Prediction { get; } = null;
        public int? Number { get; } = null;

        public Bet(int amount, BettingOn? prediction)
        {
            Amount = amount;
            Prediction = prediction;
        }

        public Bet(int amount, int number)
        {
            Amount = amount;
            Number = number;
        }

        public override string ToString()
        {
            var Out = "";
            Out += $"Amount: {Amount} Bet: ";
            if (Prediction != null && Number == null)
            {
                if (Prediction == BettingOn.Even)
                {
                    Out += "The total will be an even number.";
                }
                else if (Prediction == BettingOn.Odd)
                {
                    Out += "The total will be an odd number.";
                }
                else if (Prediction == BettingOn.Doubles)
                {
                    Out += "The dice will both have the same face value.";
                }
            }
            else if (Number != null && Prediction == null)
            {
                Out += $"The total will be {Number}.";
            }
            else
            {
                Out += "Number and Prediction are both null.";
            }
            return Out;
        }
    }
}