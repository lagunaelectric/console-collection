﻿using System;
using System.Collections.Generic;
using static System.Console;

namespace LagunaConsoleCollection.Games.DiceGames.TwoDiceGambler
{
    static class Game
    {
        #region Constants

        /// <summary>
        /// The game title
        /// </summary>
        const string GAME_TITLE = "Dice Gamble";

        /// <summary>
        /// The welcome message
        /// </summary>
        const string WELCOME_MESSAGE = GAME_TITLE
            + "! The time killing game where you bet on the outcome of dice! =D";

        #endregion Constants

        #region Read-only Fields

        /// <summary>
        /// An easy-to-use newline, for your coding convenience :D
        /// </summary>
        static readonly string n = Environment.NewLine;

        /// <summary>
        /// The full help text.
        /// </summary>
        static readonly string[] Help =  {
            $"To get started, type \"bet <cash amount> <thing to bet on>\"."
                + n
                + n + "You can bet any amount of cash as long as you have it to bet."
                + n + "You could even type \"all\", \"half\", or \"fourth\" instead of a cash amount."
                + n
                + n + "Things you can bet on:"
                + n + "even : Bet that the number is even."
                + n + "odd : Bet the number is odd."
                + n + "doubles : Bet that both dice will have the same value."
                + n + "<Whole number between 2 and 12> : Bet on the exact number."
                + n
                + n + "After your bets are placed, you can type \"roll\" to roll the dice.",
            "You can only place one bet at a time, but you can bet on multiple things at once."
            };

        #endregion Read-only Fields

        #region Properties

        /// <summary>
        /// Gets the current set of dice.
        /// </summary>
        /// <value>
        /// The current dice faces and their sum.
        /// </value>
        static string CurrentDice
            => $"First Die: {dice.First.FaceValue}, Second Die: {dice.Second.FaceValue}, Total: {dice.Total}";

        /// <summary>
        /// List of all bets.
        /// </summary>
        /// <value>
        /// The bets.
        /// </value>
        static List<Bet> Bets { get; set; } = new List<Bet>();

        #endregion Properties

        #region Private Fields

        static readonly ConsoleColor HELP_COLOR = ConsoleColor.DarkCyan;
        static readonly ConsoleColor ANGLE_BRACE_COLOR = ConsoleColor.Green;
        static readonly ConsoleColor DIVIDER_COLOR = ConsoleColor.Yellow;
        static readonly ConsoleColor NAME_COLOR = ConsoleColor.Cyan;
        static readonly ConsoleColor MONEY_COLOR = ConsoleColor.DarkGreen;
        static readonly ConsoleColor WINS_COLOR = ConsoleColor.Green;
        static readonly ConsoleColor LOSSES_COLOR = ConsoleColor.Red;
        static PairOfDice dice = new PairOfDice();
        static string playerName;
        static int playerWinCount;
        static int playerLoseCount;
        static int playerCash = 500;
        static bool isRunning;
        static int thePot;
        static bool hasLost;

        #endregion Private Fields

        public static void Start()
        {
            Clear();
            Title = "Dice Gamble - 2 Dice";
            WriteLine("What's your name?" + n + n + n);
            playerName = ReadLine();
            Clear();
            WriteLine(WELCOME_MESSAGE);
            ForegroundColor = HELP_COLOR;
            WriteLine(Help[0] ?? "Help is undefined");
            ResetColor();
#if DEBUG
            WriteLine($"Starting Dice:{n}{CurrentDice}");
#endif
            isRunning = true;
            Run();
        }

        public static void Run()
        {
            WritePreLine();
            while (isRunning)
            {
                if (!hasLost)
                {
                    if (HandleInput(ReadLine()))
                        WritePreLine();
                }
                else
                {
                    Write("Out of money!");
                    ReadLine();
                    Clear();
                    WriteLine("Bust! You're out of money!");
                    WriteLine(n);
                    WriteLine("Press Spacebar to play again, or any other key to exit.");
                    var key = ReadKey().Key;
                    if (key == ConsoleKey.Spacebar)
                    {
                        hasLost = false;
                        Restart();
                    }
                    else
                    {
                        Environment.Exit(0);
                    }
                }
            }
        }

        static void Restart()
        {
            playerCash = 500;
            playerWinCount = 0;
            playerLoseCount = 0;
            dice = new PairOfDice();
            Bets = new List<Bet>();
            thePot = 0;
            playerName = "John Doe";
            Clear();
            Start();
        }

        /// <summary>
        /// The command parsing system.
        /// </summary>
        /// <param name="userInput">The users commands.</param>
        static bool HandleInput(string userInput)
        {
            //Split userInput into an array for easier handling.
            string[] cmd = userInput.Split(' ');

            switch (cmd[0])
            {
                default:
                    Clear();
                    WriteLine("Invalid command.");
                    ReadKey();
                    Clear();
                    WriteHelp(0);
                    return true;
                case "bet":
                    PlaceBet(cmd);
                    return true;

                case "roll":
                    dice.Roll();
                    Play();
                    return true;
                case "help":
                    Clear();
                    WriteHelp(0);
                    return true;
            }
        }

        static void Play()
        {
            Clear();
            WriteLine($"The Roll:{n + n + CurrentDice + n + n}");
            var totalWinnings = 0;
            foreach (Bet bet in Bets)
            {
                if (thePot > 0)
                {
                    if (bet.Number != null)
                    {
                        if (bet.Number == dice.Total)
                        {
                            WinBet(bet);
                            totalWinnings += bet.Winnings;
                        }
                        else
                        {
                            LoseBet(bet);
                        }
                    }

                    if (bet.Prediction != null)
                    {
                        var pred = bet.Prediction;
                        if (pred == BettingOn.Even)
                        {
                            if (dice.IsEven(dice.Total))
                            {
                                WinBet(bet);
                                totalWinnings += bet.Winnings;
                            }
                            else
                            {
                                LoseBet(bet);
                            }
                        }
                        else if (pred == BettingOn.Odd)
                        {
                            if (!dice.IsEven(dice.Total))
                            {
                                WinBet(bet);
                                totalWinnings += bet.Winnings;
                            }
                            else
                            {
                                LoseBet(bet);
                            }
                        }
                        else if (pred == BettingOn.Doubles)
                        {
                            if (dice.First.FaceValue == dice.Second.FaceValue)
                            {
                                WinBet(bet);
                                totalWinnings += bet.Winnings;
                            }
                            else
                            {
                                LoseBet(bet);
                            }
                        }
                    }
                }
                else
                {
                    WriteLine(n + n + "No bets placed :(");
                }
            }
            hasLost |= playerCash <= 0;
            WriteLine(n);
            WriteLine($"Total Winnings:{totalWinnings}");
            Bets.Clear();
            thePot = 0;
        }

        static void WinBet(Bet bet)
        {
            playerCash += bet.Winnings;
            playerWinCount++;
            ForegroundColor = WINS_COLOR;
            WriteLine(bet.ToString() + $" Winnings:{bet.Winnings}");
            ResetColor();
        }

        static void LoseBet(Bet bet)
        {
            playerLoseCount++;
            ForegroundColor = LOSSES_COLOR;
            WriteLine(bet.ToString());
            ResetColor();
        }

        static void PlaceBet(string[] args)
        {
            var betAmount = 0;
            if (int.TryParse(args[1], out _))
                betAmount = int.Parse(args[1]);
            else if (args[1] == "half")
                betAmount = playerCash / 2;
            else if (args[1] == "fourth")
                betAmount = playerCash / 4;
            else if (args[1] == "all" || betAmount > playerCash)
                betAmount = playerCash;
            if (betAmount >= 0 && betAmount <= playerCash)
            {
                if (args[2] == "even")
                {
                    Bets.Add(new Bet(betAmount, BettingOn.Even));
                    playerCash -= betAmount;
                }
                else if (args[2] == "odd")
                {
                    Bets.Add(new Bet(betAmount, BettingOn.Odd));
                    playerCash -= betAmount;
                }
                else if (args[2] == "doubles" || args[2] == "dubs")
                {
                    Bets.Add(new Bet(betAmount, BettingOn.Doubles));
                    playerCash -= betAmount;
                }
                else if (int.TryParse(args[2], out _))
                {
                    if (int.Parse(args[2]) >= 2 && int.Parse(args[2]) <= 12)
                    {
                        Bets.Add(new Bet(betAmount, int.Parse(args[2])));
                        playerCash -= betAmount;
                    }
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(betAmount), $"{betAmount}",
                    "Value must be greater than zero and less than the players cash amount");
            }
            Clear();
            ForegroundColor = ConsoleColor.Cyan;
            foreach (Bet bet in Bets)
            {
                WriteLine(bet.ToString());
                thePot += bet.Amount * 2;
            }
            ForegroundColor = ConsoleColor.DarkYellow;
            WriteLine($"{n + n}Pot:{thePot}");
            ResetColor();
        }

        #region Writers

        static void WriteStr(string message, ConsoleColor messageColor)
        {
            ForegroundColor = messageColor;
            Write(message);
        }

        static void WriteHelp(int index)
        {
            ForegroundColor = HELP_COLOR;
            Write(Help[index]);
        }

        static void WritePreLine()
        {
            WriteLine(n);
            WriteBracket(true);
            WriteDivider();
            WriteStr($" {playerName} ", NAME_COLOR);
            WriteDivider();
            WriteStr($" $:{playerCash} ", MONEY_COLOR);
            WriteDivider();
            WriteStr($" W:{playerWinCount} ", WINS_COLOR);
            WriteDivider();
            WriteStr($" L:{playerLoseCount} ", LOSSES_COLOR);
            WriteDivider();
            WriteBracket(false);
            ResetColor();
        }

        static void WriteBracket(bool isOpening)
        {
            string bracket = (isOpening) ? "<" : "> ";
            WriteStr(bracket, ANGLE_BRACE_COLOR);
        }

        static void WriteDivider()
        {
            WriteStr("|", DIVIDER_COLOR);
        }

        #endregion Writers
    }
}