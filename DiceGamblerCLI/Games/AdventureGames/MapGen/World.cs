﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LagunaConsoleCollection.Games.AdventureGames.MapGen
{
    internal class World
    {
        #region Private Fields

        /// <summary>
        /// The grid for the world.
        /// </summary>
        Tile[][] grid;

        /// <summary>
        /// The height of the map.
        /// </summary>
        readonly int height;

        /// <summary>
        /// The width of the map.
        /// </summary>
        int width;

        /// <summary>
        /// The name of the world.
        /// </summary>
        String name;

        /// <summary>
        /// A list of all possible town names.
        /// </summary>
        HashSet<string> townNames;


        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// The grid for the world.
        /// </summary>
        public Tile[][] Grid { get => grid; set => grid = value; }

        public string Name { get => name; set => name = value; }

        #endregion Public Properties

        /// <summary>
        /// Initializes a new instance of the <see cref="World"/> class.
        /// </summary>
        /// <param name="aWidth">Total width of the world.</param>
        /// <param name="aHeight">Total height of the world.</param>
        public World(int aWidth, int aHeight)
        {
            name = "Solace";
            width = aWidth;
            height = aHeight;
            grid = new Tile[width][];
            for (int i = 0; i < width; i++) { grid[i] = new Tile[height]; }
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    grid[x][y] = new Tile();
                }
            }

            townNames = new HashSet<string>()
            {
                "Atlanta",
                "London",
                "Paris",
                "Madrid",
                "Dubai",
                "Tokyo",
                "Rio",
                "Atlantis",
                "Detroit",
                "Manchester",
                "Essex",
                "Denver"
            };
        }


        #region Public Methods

        //------------------------------------------FillWorld In-Depth------------------------------------------------------------
        // fillworld lets you create a blank map of a particular terrain, and control what types of terrain exist at the map edge.
        // If you want islands, supply the parameters 0,0,0,0.. otherwise
        // If you want other terrain types at the edge of the map, supply that terrain type,
        // and then use the lake feature to drop rectangular lakes all over the map.
        // This is because the world generator only places its stuff on water tiles, so to get
        // a lot of variety when making the actual world, lots of lakes is good
        // the world generator is ideally suited to making islands, but these extra terrain types
        // and lakes will allow for interesting land locked maps too
        // There is a miniisland value to try and reduce the obviously rectangular nature
        // of default lakes, treat it as a percentage of lake that is water.
        //------------------------------------------------------------------------------------------------------------------------
        // large number = more grass islands
        //-----------------------------------
        /// <summary>
        /// Fills a map with a terrain type, and optionally alows rectangular lakes to be placed on it
        /// </summary>
        public void fillworld(int seed, int terraintype, int lakedensity, int lakesize, int miniislands)

        {
            Random ran = new Random(seed); // first fill the whole map with the parameter terraintype

            for (int b = 0; b < height; b++)
            {
                for (int a = 0; a < width; a++)
                {
                    grid[a][b].BuildingID = 0;
                    if (terraintype == 0)
                    {
                        grid[a][b].ID = 0;
                        grid[a][b].Symbol = 'W';
                        grid[a][b].Name = "Water";
                        grid[a][b].IDExtra = ran.Next(0, 2);
                    }
                    if (terraintype == 1)
                    {
                        grid[a][b].ID = 1;
                        grid[a][b].Symbol = '&';
                        grid[a][b].Name = "Grasslands";
                        grid[a][b].IDExtra = ran.Next(0, 2);
                    }
                    if (terraintype == 2)
                    {
                        grid[a][b].ID = 2;
                        grid[a][b].Symbol = 'V';
                        grid[a][b].Name = "Mountain";
                        grid[a][b].IDExtra = ran.Next(0, 2);
                    }
                    if (terraintype == 3)
                    {
                        grid[a][b].ID = 3;
                        grid[a][b].Symbol = '#';
                        grid[a][b].Name = "Forest";
                        grid[a][b].IDExtra = ran.Next(0, 2);
                    }
                }
            }
            for (int k = 0; k < lakedensity; k++) // then add lakedensity number of lakes, each one sized based on a random number generated from lakesize
            {
                int tempX = ran.Next(0, width);
                int tempY = ran.Next(0, height);
                int lakesizeup = ran.Next(0, lakesize);
                int lakesizedown = ran.Next(0, lakesize);
                int lakesizeleft = ran.Next(0, lakesize);
                int lakesizeright = ran.Next(0, lakesize);
                for (int i = tempX - lakesizeleft; i < tempX + lakesizeright; i++)
                {
                    for (int j = tempY - lakesizeup; j < tempY + lakesizedown; j++)
                    {
                        if (i > -1 && i < width)
                        {
                            if (j > -1 && j < height)
                            {
                                // these lakes can contain mini grass islands (i.e. only set a 100% - miniislands percentage to water)
                                if (miniislands > 0 && ran.Next(1, 100) > miniislands)
                                {
                                    grid[i][j].ID = 0;
                                    grid[i][j].Symbol = 'W';
                                    grid[i][j].Name = "Water";
                                    grid[i][j].IDExtra = ran.Next(0, 2);
                                }
                                else if (miniislands == 0) // or just be water
                                {
                                    grid[i][j].ID = 0;
                                    grid[i][j].Symbol = 'W';
                                    grid[i][j].Name = "Water";
                                    grid[i][j].IDExtra = ran.Next(0, 2);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generates the world by creating a matrix of tiles randomly.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void generateWorld(int x, int y)
        {
            Random ran = new Random();
            grid[x][y].ID = 2; //Places home
            grid[x][y].BuildingID = 1;
            grid[x][y].Symbol = 'H';
            grid[x][y].Name = "Home";

            //grid[x][y].setDesc("Your cabin looks extremely cozy in the middle of the forest. "
            //    + "There's a little bit of smoke still rising out of the chimney from the night's fire. "
            //    + "A pile of fire wood rests just out of the front door, sheltered from the rain by an awning "
            //    + "you purchased off a traveling merchant. Your axe is no longer resting next to the pile.", 0);

            //grid[x][y].setDesc("You notice a small group of squirrels running through the trees overhead, "
            //    + "dropping twigs and leaves around you. One leaves the group to dig a small hole at the bottom "
            //    + "of a tree, burying acorns and nuts that it had seemed to pull out of nowhere.", 1);

            //grid[x][y].setDesc("The wind blows gently past you, picking up fallen leaves and dropping "
            //    + "them off close to where they once were. Dark clouds are beginning to gather overhead, "
            //    + "blocking out the sun and giving a rather gloomy atmosphere. You notice some small rodents "
            //    + "scurrying underneath your cabin to protect themselves from the weather.", 2);

            int i = 0;
            for (int k = 0; k < 15; k++)
            {
                int a = ran.Next(0, 9);
                int b = ran.Next(0, 9);
                if (grid[a][b].ID == 0)
                {
                    grid[a][b].ID = 1;
                    grid[a][b].Symbol = '&';
                    grid[a][b].Name = "Grasslands";
                    grid[a][b].IDExtra = ran.Next(0, 2);
                }
            }
            for (int b = 0; b < height; b++)
            {
                for (int a = 0; a < width; a++)
                {
                    if (grid[a][b].ID == 0)
                    {
                        if (a > 0) { if (grid[a - 1][b].ID > 0) { i++; } }
                        if (a < width - 1) { if (grid[a + 1][b].ID > 0) { i++; } }
                        if (b > 0) { if (grid[a][b - 1].ID > 0) { i++; } }
                        if (b < height - 1) { if (grid[a][b + 1].ID > 0) { i++; } }

                        int prob = 15 + 60 * i;

                        if (ran.Next(1, 138) <= prob)
                        {
                            int temp = ran.Next(1, 10);

                            if (temp < 5)
                            {
                                grid[a][b].ID = 1;
                                grid[a][b].Symbol = '&';
                                grid[a][b].Name = "Grasslands";
                                grid[a][b].IDExtra = ran.Next(0, 2);
                            }
                            else if (temp < 7)
                            {
                                grid[a][b].ID = 2;
                                grid[a][b].Symbol = 'V';
                                grid[a][b].Name = "Mountain";
                                grid[a][b].IDExtra = ran.Next(0, 2);
                            }
                            else
                            {
                                grid[a][b].ID = 3;
                                grid[a][b].Symbol = '#';
                                grid[a][b].Name = "Forest";
                                grid[a][b].IDExtra = ran.Next(0, 2);
                            }
                        }
                        else
                        {
                            grid[a][b].ID = 0;
                            grid[a][b].Symbol = 'W';
                            grid[a][b].Name = "Water";
                            grid[a][b].IDExtra = ran.Next(0, 2);
                        }
                        grid[a][b].Difficulty = (int)(Math.Abs(a - x) + Math.Abs(b - y) / 2);
                        i = 0;
                    }
                }
            }
            while (i < 7) //makes castles
            {
                int tempX = ran.Next((i * 10), ((i + 1) * 10));
                int tempY = ran.Next(0, height - 1);
                if (grid[tempX][tempY].ID > 0)
                {
                    grid[tempX][tempY].BuildingID = 2;
                    grid[tempX][tempY].Symbol = 'M';
                    grid[tempX][tempY].Name = "Castle";
                    grid[tempX][tempY].IDExtra = ran.Next(0, 2);
                    i++;
                }
            }
            i = 0;
            while (i < 3) //makes towns
            {
                int tempX = ran.Next((i * 20), ((i + 1) * 20));
                int tempY = ran.Next(0, height - 1);
                if (grid[tempX][tempY].ID > 0 && grid[tempX][tempY].BuildingID == 0)
                {
                    grid[tempX][tempY].BuildingID = 3;
                    grid[tempX][tempY].Symbol = 'T';
                    grid[tempX][tempY].Name = "Town";
                    grid[tempX][tempY].IDExtra = ran.Next(0, 2);
                    i++;
                }
            }
            i = 0;
        }

        //--------------------------------------GenerateWorld2 In-Depth------------------------------------------------
        // generate world requires the relative sizes of mountains, forest and grass,
        // the number of areas (density) there these areas of mountain/forest/grass to try and make
        // and how far from the edge of the map it should begin trying to do this
        // (10 is good for a large island, 5 is good for land maps).
        // The generator will attempt to create a 'density' number of terrain objects on water tiles.
        // It works best producing a large island, but other types of map are possible.
        // The number of water tiles is determined by fillworld, so if there are not many water areas,
        // then this map generator will not generate many terrain items.
        // The basic staring point is that mountains are surrounded by forest, that is surrounded by grass,
        // that is created on a fillworld generated water tile, and it tries to do this 'density' number of times.
        // There is some randomness in the thickness of the respective areas of mountain forest and grass,
        // which means you can virtually eliminate mountains, very little forest and hardly any grass,
        // and also have mountains by the coast or adjacent to grass, and forest right on the coast.
        //------------------------------------------------------------------------------------------------------------
        // For example:
        // fillworld(0,0,0,0) followed by generateworld2(3,6,9,65,10,10) tends to produce one large island which is
        // grassy with large forested areas and small numbers of mountains, there may be lakes, there will be bays.
        //------------------------------------------------------------------------------------------------------------
        // fillworld(0,0,0,0) followed by generateworld2(1,9,1,65,10,10) tends to produce heavily forested large
        // and small islands, some coastal grass, occasional lakes
        //------------------------------------------------------------------------------------------------------------
        // fillworld(0,0,0,0) followed by generateworld2(2,7,7,140,5,5) tends to produce one huge island with equal
        // forest and grass, some mountains, occasional lakes
        //------------------------------------------------------------------------------------------------------------
        // fillworld(1,30,15,0) followed by generateworld2(2,7,7,100,5,5) tends to produce a grassy map with huge
        // forests and medium lakes, and a few mountains
        //------------------------------------------------------------------------------------------------------------
        // fillworld(3,30,15,0) followed by generateworld2(4,7,4,100,5,5) tends to produce a forest map with
        // large grass patches and many lakes and mountains
        //------------------------------------------------------------------------------------------------------------
        // fillworld(1,30,15,0) followed by generateworld2(4,7,4,100,5,5) tends to produce a grass map with
        // large forests splotches and many lakes and mountains
        //------------------------------------------------------------------------------------------------------------
        // fillworld(0,0,0,0) followed by generateworld2(4,7,4,30,5,5) tends to produce a few heavily
        // forested islands with mountains
        //------------------------------------------------------------------------------------------------------------
        // fillworld(0,0,0,0) followed by generateworld2(1,1,5,50,5,5) tends to produce a number
        // of small grass only islands
        //------------------------------------------------------------------------------------------------------------
        // fillworld(1,30,10,10) followed by generateworld2(3,6,1,50,5,5) tends to produce a grassy area with many lakes
        // and forests, some mountains, and 10% of the lakes are converted to grass (makes islands and roughens lake edges)
        //------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Creates a map with large contiguous areas of rough edged water, grass and forest, and some mountains.
        /// </summary>
        /// <param name="mountains"></param>
        /// <param name="forest"></param>
        /// <param name="grass"></param>
        /// <param name="density"></param>
        /// <param name="indent1"></param>
        /// <param name="indent2"></param>
        public void generateworld2(int mountains, int forest, int grass, int density, int indent1, int indent2)
        {
            Random ran = new Random(42);
            int terrainID1 = -1; // forest ID (each contiguous area of forest has a number)
            int terrainID2 = 1;
            for (int k = 0; k < density; k++) // density is the number of times we want to create mountain/forest.grass features, so loop this number of times
            {
                int tempX = ran.Next(indent1, width - indent1); // get coordinates to start the feature
                int tempY = ran.Next(indent2, height - indent2);
                int mountainsizeup = ran.Next(0, mountains); // then use the parameters mountain forest and grass to generate random values offset up, down, left and right from this point for the 3 terrain types
                int forestsizeup = ran.Next(0, forest);
                int grasssizeup = ran.Next(0, grass);
                int mountainsizedown = ran.Next(0, mountains);
                int forestsizedown = ran.Next(0, forest);
                int grasssizedown = ran.Next(0, grass);
                int mountainsizeleft = ran.Next(0, mountains);
                int forestsizeleft = ran.Next(0, forest);
                int grasssizeleft = ran.Next(0, grass);
                int mountainsizeright = ran.Next(0, mountains);
                int forestsizeright = ran.Next(0, forest);
                int grasssizeright = ran.Next(0, grass);

                // draw a grass rectangle based on the random numbers
                for (int i = tempX - mountainsizeleft - forestsizeleft - grasssizeleft; i < tempX + mountainsizeright + forestsizeright + grasssizeright; i++)
                {
                    int crinkle3 = ran.Next(0, 2) - 1; // crinkle the left to right edges a little bit, gives a rougher edge.
                    int crinkle4 = ran.Next(0, 2) - 1;
                    for (int j = tempY - mountainsizeup - forestsizeup - grasssizeup - crinkle3; j < tempY + mountainsizedown + forestsizedown + grasssizedown + crinkle4; j++)
                    {
                        if (i > 2 && i < width - 2) // ensure the grass rectangle does not extend to the edge of the map (should fill map be set up to generate islands).
                        {
                            if (j > 2 && j < height - 2)
                            {
                                if (grid[i][j].Symbol == 'W') // and so if the tile currently selected in the grass rectangle is water, replace with grass, means that previously drawn land features are not erased, this will cause two adjacent grass areas to merge, forming a contiguous grassy area.
                                {
                                    grid[i][j].ID = 1;
                                    grid[i][j].Symbol = '&';
                                    grid[i][j].Name = "Grasslands";
                                    grid[i][j].IDExtra = 0; //grasslands have no name
                                }
                            }
                        }
                    }
                }
                // now do something similar for forest rectangles, note that forest areas can be equal to or smaller than the grass rectangles they exist in
                for (int i = tempX - mountainsizeleft - forestsizeleft; i < tempX + mountainsizeright + forestsizeright; i++)
                {
                    int crinkle3 = ran.Next(0, 2) - 1; // again roughen up the left to right edges
                    int crinkle4 = ran.Next(0, 2) - 1;
                    for (int j = tempY - mountainsizeup - forestsizeup - crinkle3; j < tempY + mountainsizedown + forestsizedown + crinkle4; j++)
                    {
                        if (i > 3 && i < width - 3) // and again, make sure these things do not reach the edge of the map in case we are generating an island map
                        {
                            if (j > 3 && j < height - 3)
                            {
                                if (grid[i][j].Symbol == '&') // this time only replace grassland with forest, this means that should this forest rectangle end up bing adjacent to another forest rectangle, the two will merge making a larger forest. This should result in large swathes of forest with suitable settings, and because the grassland size could theoretically be 0, forests may extend straight to water.
                                {
                                    grid[i][j].ID = 3;
                                    grid[i][j].Symbol = '#';
                                    grid[i][j].Name = "Forest";
                                    grid[i][j].IDExtra = terrainID1; // decide which forest this belongs to
                                }
                            }
                        }
                    }
                }
                // and finally mountains, using the same ideas as before
                for (int i = tempX - mountainsizeleft; i < tempX + mountainsizeright; i++)
                {
                    int crinkle3 = ran.Next(0, 2) - 1; // again roughen up the left to right edges
                    int crinkle4 = ran.Next(0, 2) - 1;
                    for (int j = tempY - mountainsizeup - crinkle3; j < tempY + mountainsizedown + crinkle4; j++)
                    {
                        if (i > 4 && i < width - 4) // and again, make sure these things do not reach the edge of the map in case we are generating an island map
                        {
                            if (j > 4 && j < height - 4)
                            {
                                if (grid[i][j].Symbol == '#') // now we replace only forest with mountains, again it can allow neighboring mountains to link up forming a mountain chain, though it is rare you'd want this on a map, however it can result in clusters of mountains. As before, mountains may end up adjacent to water or grass if the grassland size is zero
                                {
                                    grid[i][j].ID = 2;
                                    grid[i][j].Symbol = 'V';
                                    grid[i][j].Name = "Mountain";
                                    grid[i][j].IDExtra = terrainID2; // unassigned name at this point
                                }
                            }
                        }
                    }
                }
                // now to crinkle the edges a little bit, Slartibartfast won an Award for Norway, this is my version of his fjord generator code :D
                for (int j = 1; j < height - 1; j++) // go from top to bottom of the map
                {
                    var tileId = this.grid[1][j].ID; // find the tile in column 1 of this row
                    var justchangedone = false;
                    for (int i = 2; i < width - 2; i++) // and then go check the next tile to the right of it
                    {
                        var nexttileId = this.grid[i][j].ID; // find what it is and store it
                        var nexttilesymbol = this.grid[i][j].Symbol;
                        var nexttilename = this.grid[i][j].Name;
                        var nextIDextra = this.grid[i][j].IDExtra;
                        if (tileId != nexttileId && !justchangedone) // if we haven't just changed a tile, and this tile is different to the left most tile
                        {
                            int crinkle1 = ran.Next(0, 2); // we randomly choose to modify a tile up to two tiles further to the right with the settings for the current tile
                            this.grid[i + crinkle1][j].ID = nexttileId;
                            this.grid[i + crinkle1][j].Symbol = nexttilesymbol;
                            this.grid[i + crinkle1][j].Name = nexttilename;
                            this.grid[i + crinkle1][j].IDExtra = nextIDextra;
                            justchangedone = true; // and ensure that nothing happens to the next tile on the right
                        }
                        else
                        {
                            justchangedone = false; // tell the system to resume changing tiles if the above if conditions does not trigger
                        }
                    }
                }
                // again top to bottom
                for (int j = 1; j < height - 1; j++)
                {
                    var tileId = this.grid[width - 1][j].ID; // pick the tile near the right edge of the map
                    var justchangedone = false;
                    for (int i = width - 1; i > 1; i--) // and repeat the above crinkling algorithm but this time from right to left
                    {
                        var nexttileId = this.grid[i][j].ID;
                        var nexttilesymbol = this.grid[i][j].Symbol;
                        var nexttilename = this.grid[i][j].Name;
                        var nextIDextra = this.grid[i][j].IDExtra;
                        if (tileId != nexttileId && !justchangedone)
                        {
                            int crinkle1 = ran.Next(0, 2);
                            this.grid[i - crinkle1][j].ID = nexttileId;
                            this.grid[i - crinkle1][j].Symbol = nexttilesymbol;
                            this.grid[i - crinkle1][j].Name = nexttilename;
                            this.grid[i - crinkle1][j].IDExtra = nextIDextra;
                            justchangedone = true;
                        }
                        else
                        {
                            justchangedone = false;
                        }
                    }
                }
                // now from left to right
                for (int j = 1; j < width - 1; j++)
                {
                    var tileId = this.grid[j][1].ID; //pick a tile along the top of the map
                    var justchangedone = false;
                    for (int i = 2; i < height - 2; i++) // and repeat the above crinkling algorithm, but this time from top to bottom.
                    {
                        var nexttileId = this.grid[j][i].ID;
                        var nexttilesymbol = this.grid[j][i].Symbol;
                        var nexttilename = this.grid[j][i].Name;
                        var nextIDextra = this.grid[j][i].IDExtra;
                        if (tileId != nexttileId && !justchangedone)
                        {
                            int crinkle2 = ran.Next(0, 1);
                            this.grid[j][i + crinkle2].ID = nexttileId;
                            this.grid[j][i + crinkle2].Symbol = nexttilesymbol;
                            this.grid[j][i + crinkle2].Name = nexttilename;
                            this.grid[j][i + crinkle2].IDExtra = nextIDextra;
                            justchangedone = true;
                        }
                        else
                        {
                            justchangedone = false;
                        }
                    }
                }
                // left to right again
                for (int j = 1; j < width - 1; j++)
                {
                    var tileId = this.grid[j][height - 1].ID; // but this time a tile along the bottom of the map
                    var justchangedone = false;
                    for (int i = height - 2; i > 2; i--) // and the same algorithm, but crinkling form bottom to top
                    {
                        var nexttileId = this.grid[j][i].ID;
                        var nexttilesymbol = this.grid[j][i].Symbol;
                        var nexttilename = this.grid[j][i].Name;
                        var nextIDextra = this.grid[j][i].IDExtra;
                        if (tileId != nexttileId && !justchangedone)
                        {
                            int crinkle2 = ran.Next(0, 1);
                            this.grid[j][i - crinkle2].ID = nexttileId;
                            this.grid[j][i - crinkle2].Symbol = nexttilesymbol;
                            this.grid[j][i - crinkle2].Name = nexttilename;
                            this.grid[j][i - crinkle2].IDExtra = nextIDextra;
                            justchangedone = true;
                        }
                        else
                        {
                            justchangedone = false;
                        }
                    }
                }
            }
            // now we need castles
            int timetogiveup = 0;
            for (int i = 0; i < 30; i++) //makes up to 30 castles in forest or mountains with no adjacent buildings
            {
                timetogiveup++; // we have 200 attempts at this at most, for maps with little suitable terrain, this allows the game to reduce the instances of castles
                int tempX = ran.Next(1, width - 1); // choose a place
                int tempY = ran.Next(1, height - 1);
                if (grid[tempX][tempY].ID > 1 && grid[tempX - 1][tempY - 1].BuildingID == 0 && grid[tempX][tempY - 1].BuildingID == 0 && grid[tempX + 1][tempY - 1].BuildingID == 0 && grid[tempX - 1][tempY].BuildingID == 0 && grid[tempX][tempY].BuildingID == 0 && grid[tempX + 1][tempY].BuildingID == 0 && grid[tempX - 1][tempY + 1].BuildingID == 0 && grid[tempX][tempY + 1].BuildingID == 0 && grid[tempX + 1][tempY + 1].BuildingID == 0)
                { // if its a forest or mountain and no adjacent tile is already set to a building type then set it to a castle (mountain) or fort (forest)
                    if (grid[tempX][tempY].ID == 2)
                    {
                        grid[tempX][tempY].BuildingID = 2;
                        grid[tempX][tempY].Symbol = 'M';
                        grid[tempX][tempY].Name = "Castle";
                        grid[tempX][tempY].IDExtra = 0;
                    }
                    else
                    {
                        grid[tempX][tempY].BuildingID = 2;
                        grid[tempX][tempY].Symbol = 'M';
                        grid[tempX][tempY].Name = "Fort";
                        grid[tempX][tempY].IDExtra = 0;
                    }
                }
                else if (timetogiveup < 200) // so assuming its not time to give up
                {
                    i--; // try again by gong back one loop index step
                }
            }
            timetogiveup = 0; // now do the same kind of thing for towns, we want up to 40 towns in the grass or forest
            for (int i = 0; i < 40; i++) //makes towns on grass or forest nothing adjacent
            {
                timetogiveup++; // again it tries this 200 times at most
                int tempX = ran.Next(1, width - 1);
                int tempY = ran.Next(1, height - 1);
                if ((grid[tempX][tempY].ID == 1 || grid[tempX][tempY].ID == 3) && grid[tempX - 1][tempY - 1].BuildingID == 0 && grid[tempX][tempY - 1].BuildingID == 0 && grid[tempX + 1][tempY - 1].BuildingID == 0 && grid[tempX - 1][tempY].BuildingID == 0 && grid[tempX][tempY].BuildingID == 0 && grid[tempX + 1][tempY].BuildingID == 0 && grid[tempX - 1][tempY + 1].BuildingID == 0 && grid[tempX][tempY + 1].BuildingID == 0 && grid[tempX + 1][tempY + 1].BuildingID == 0)
                { // if we have a grassland or mountain, and there are no adjacent buildings
                    if (grid[tempX - 1][tempY].ID != 0 && grid[tempX + 1][tempY].ID != 0 && grid[tempX][tempY + 1].ID != 0 && grid[tempX][tempY - 1].ID != 0)
                    { // and there is no adjacent water
                        grid[tempX][tempY].BuildingID = 3;
                        grid[tempX][tempY].Symbol = 'T';

                        var rnd = new Random();
                        var townInt = rnd.Next(0, townNames.Count);
                        grid[tempX][tempY].Name = townNames.ElementAtOrDefault(townInt);
                        grid[tempX][tempY].IDExtra = 0;
                    }
                }
                else if (timetogiveup < 200) // same code for deciding when th give up
                {
                    i--;
                }
            }
            timetogiveup = 0;
            for (int i = 0; i < 20; i++) //makes towns adjacent to the water on grass or forest
            {
                timetogiveup++; // i think you get the idea now
                int tempX = ran.Next(1, width - 1);
                int tempY = ran.Next(1, height - 1);
                if ((grid[tempX][tempY].ID == 1 || grid[tempX][tempY].ID == 3) && grid[tempX - 1][tempY - 1].BuildingID == 0 && grid[tempX][tempY - 1].BuildingID == 0 && grid[tempX + 1][tempY - 1].BuildingID == 0 && grid[tempX - 1][tempY].BuildingID == 0 && grid[tempX][tempY].BuildingID == 0 && grid[tempX + 1][tempY].BuildingID == 0 && grid[tempX - 1][tempY + 1].BuildingID == 0 && grid[tempX][tempY + 1].BuildingID == 0 && grid[tempX + 1][tempY + 1].BuildingID == 0)
                { // this time not only ensure there are no adjacent buildings
                    if (grid[tempX - 1][tempY].ID == 0 || grid[tempX + 1][tempY].ID == 0 || grid[tempX][tempY + 1].ID == 0 || grid[tempX][tempY - 1].ID == 0)
                    { // but that there is adjacent water
                        grid[tempX][tempY].BuildingID = 3;
                        grid[tempX][tempY].Symbol = 'T';
                        grid[tempX][tempY].Name = "Port";
                        grid[tempX][tempY].IDExtra = 0;
                    }
                    else if (timetogiveup < 200) // and the same code for deciding when to give up
                    {
                        i--;
                    }
                }
                else if (timetogiveup < 200)
                {
                    i--;
                }
            }
        }

        public int getBuilding(int x, int y) => grid[x][y].BuildingID;

        public int getHeight() => height;

        public int getId(int x, int y) => grid[x][y].ID;

        public string getName(int x, int y) => grid[x][y].Name;

        public int getWidth() => width;

        public void placehome(int x, int y)
        {
            grid[x][y].ID = 2; //Places home
            grid[x][y].BuildingID = 1;
            grid[x][y].Symbol = 'H';
            grid[x][y].Name = "Home";
            grid[x][y].BuildingExtra = 100;
        }

        /// <summary>
        /// Loops through all of the tiles and draws their symbols to form a map
        /// </summary>
        public void draw()
        {
            Console.ResetColor();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (grid[x][y].BuildingID == 2)
                    {
                        drawSymbol(ConsoleColor.Magenta, ConsoleColor.DarkRed, grid, x, y);
                    }
                    else if (grid[x][y].BuildingID == 3)
                    {
                        drawSymbol(ConsoleColor.Black, ConsoleColor.Magenta, grid, x, y);
                    }
                    else if (grid[x][y].BuildingID == 1)
                    {
                        drawSymbol(ConsoleColor.White, ConsoleColor.Black, grid, x, y);
                    }
                    else if (grid[x][y].Symbol == 'W')
                    {
                        drawSymbol(ConsoleColor.DarkCyan, ConsoleColor.Cyan, grid, x, y);
                    }
                    else if (grid[x][y].Symbol == '&')
                    {
                        drawSymbol(ConsoleColor.Green, ConsoleColor.DarkGreen, grid, x, y);
                    }
                    else if (grid[x][y].Symbol == 'V')
                    {
                        drawSymbol(ConsoleColor.Black, ConsoleColor.DarkGray, grid, x, y);
                    }
                    else if (grid[x][y].Symbol == '#')
                    {
                        drawSymbol(ConsoleColor.DarkGreen, ConsoleColor.Black, grid, x, y);
                    }
                    //else { Console.Write(grid[x][y].Symbol); }
                }
            }
            Console.WriteLine();
            Console.WriteLine("W: Water; &: Grasslands; V: Mountain; #: Forest");
            Console.WriteLine("@: Player; H: Home; M: Castle; T: Town");
        }

        public void drawSymbol(ConsoleColor foreGround, ConsoleColor backGround, Tile[][] grid, int x, int y)
        {
            Console.ForegroundColor = foreGround;
            Console.BackgroundColor = backGround;
            Console.Write(grid[x][y].Symbol.ToString());
            Console.ResetColor();
        }

        public void setBuilding(int x, int y, int newId)
        {
            grid[x][y].BuildingID = newId;
        }

        public void setId(int x, int y, int newId)
        {
            grid[x][y].ID = newId;
        }


        /// <summary>
        /// Loops through and writes up a description for each tile depending on the id, idExtra, buildingId, and buildingExtra
        /// </summary>
        public void writeDesc()
        {
            Random ran = new Random();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int rand = ran.Next(0, 101);
                    int buildingType = grid[x][y].BuildingID;

                    if (buildingType == 0)
                    {
                        int tileID = grid[x][y].ID;

                        if (tileID == 0)
                        {
                            //TODO: Water tile descriptions.
                        }
                        else if (tileID == 1)
                        {
                            //TODO: Grass tile descriptions.
                        }
                        else if (tileID == 2)
                        {
                            //TODO: Mountain tile descriptions.
                        }
                        else if (tileID == 3)
                        {
                            //TODO: Forest tile descriptions.
                        }
                    }
                    else if (buildingType == 2)
                    {
                        //TODO: Castle tile descriptions.
                    }
                    else if (buildingType == 3)
                    {
                    }
                    switch (grid[x][y].BuildingID)
                    {
                        case 0:
                            switch (grid[x][y].ID)
                            {
                                case 0:
                                    if (rand >= 33)
                                    {
                                    }
                                    break;

                                case 1:
                                    break;

                                case 2:
                                    break;

                                case 3:
                                    break;
                            }
                            break;

                        case 1:
                            break;

                        case 2:
                            break;

                        case 3:
                            break;
                    }
                }
            }
        }

        /**
         * <summary>Sets the tile's id at a given coordinate</summary>
         * <param name="x">The x coordinate of the tile</param>
         * <param name="y">The y coordinate of the tile</param>
         * <param name="newId">The tile's new id - 0:water, 1:grasslands, 2:mountain, 3:forest</param>
         */
        /**
         * <summary>Retrieves the id of a given tile.</summary>
         * <param name="x">The x coordinate of the tile</param>
         * <param name="y">The y coordinate of the tile</param>
         * <returns>Int of the tile's id - 0:water, 1:grasslands, 2:mountain, 3:forest</return>
         */
        /**
         * <summary>Sets the tile's building id at a given coordinate</summary>
         * <param name="x">The x coordinate of the tile</param>
         * <param name="y">The y coordinate of the tile</param>
         * <param name="newId">The tile's new building id - 0:nothing, 1:home, 2:castle, 3:town</param>
         */
        /**
         * <summary>Retrieves the building id of a given tile.</summary>
         * <param name="x">The x coordinate of the tile</param>
         * <param name="y">The y coordinate of the tile</param>
         * <returns>Int of the tile's building id - 0:nothing, 1:home, 2:castle, 3:town</return>
         */
        /**
         * <summary>Retrieves the player's y location on the map</summary>
         * <returns>Int of the player's y location</return>
         */
        /**
         * <summary>Retrieves the height of the map</summary>
         * <returns>Int of the map's height</return>
         */
        /**
         * <summary>Retrieves the width of the map</summary>
         * <param name="x">The tile's x location on the map</param>
         * <param name="y">The tile's y location on the map</param>
         * <returns>Int of the map's width</return>
         */

        #endregion Public Methods
    }
}