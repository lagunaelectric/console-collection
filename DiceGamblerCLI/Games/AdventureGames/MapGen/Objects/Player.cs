﻿using LagunaConsoleCollection.Games.AdventureGames.MapGen.Modules;

namespace LagunaConsoleCollection.Games.AdventureGames.MapGen.Objects
{
    class Player
    {
        public bool isOnMap;
        public Coordinate mapCoords;

        public Player()
        {
            mapCoords = new Coordinate()
            {
                X = 0,
                Y = 0
            };
            isOnMap = false;
        }

        public Tile standingOn(Tile[][] map) => map[mapCoords.X][mapCoords.Y];
    }
}
