﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LagunaConsoleCollection.Games.AdventureGames.MapGen
{
    class Tile
    {
        /// <summary>
        /// Additional information for the building
        /// </summary>
        public int BuildingExtra { get; set; }

        /// <summary>
        /// 0:none, 1:home, 2:castle, 3:town
        /// </summary>
        public int BuildingID { get; set; }

        /// <summary>
        /// Description of the tile.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Grows as tiles move from Home
        /// </summary>
        public int Difficulty { get; set; } = 1;

        /// <summary>
        /// Tile id.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Additional information for the tile
        /// </summary>
        public int IDExtra { get; set; }

        /// <summary>
        /// 0:water, 1:grasslands, 2:mountain, 3:forest
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Map symbol
        /// </summary>
        public char Symbol { get; set; } = '-';
    }
}
