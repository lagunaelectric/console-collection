﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LagunaConsoleCollection.Games.AdventureGames.MapGen
{
    public enum TerrainType
    {
        //Air = 0,
        Grass = 1,

        Dirt = 2
    }

    class Terrain
    {
        const ConsoleColor DIRT_COLOR = ConsoleColor.DarkMagenta;
        const string DIRT_DESC = "Bare ground with rocks.";
        const string DIRT_NAME = "Dirt";
        const ConsoleColor GRASS_COLOR = ConsoleColor.Green;
        const string GRASS_DESC = "Soft green grass that blows gently in the breeze.";
        const string GRASS_NAME = "Grass";
        static ConsoleColor color;
        static string desc;
        static string name;

        public Terrain()
        {
            name = "";
            desc = "";
            color = ConsoleColor.Black;
        }

        public Terrain(TerrainType type)
        {
            SetTerrain(type);
        }

        public Terrain(
            string aName,
            string aDesc,
            ConsoleColor aColor)
        {
            name = aName;
            desc = aDesc;
            color = aColor;
        }

        public ConsoleColor Color => color;
        public string Description => desc;
        public string Name => name;

        public void SetTerrain(TerrainType type)
        {
            switch (type)
            {
                case TerrainType.Grass:
                    name = GRASS_NAME;
                    desc = GRASS_DESC;
                    color = GRASS_COLOR;
                    break;

                case TerrainType.Dirt:
                    name = DIRT_NAME;
                    desc = DIRT_DESC;
                    color = DIRT_COLOR;
                    break;
            }
        }
    }
}
