﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LagunaConsoleCollection.Games.AdventureGames.MapGen
{
    class Shortcuts
    {
        #region Code Shortcuts

        public static void ChangeColor(ConsoleColor c)
        {
            Console.ForegroundColor = c;
        }

        public static void Clear()
        {
            Console.Clear();
        }

        public static void fucksGiven(int i)
        {
            Environment.Exit(i);
        }

        public static ConsoleKey KeyInput() => Console.ReadKey().Key;

        public static string LineInput() => Console.ReadLine();

        public static string Msg(string a) => a + "\n\n\n";

        /// <summary>
        /// Console.WriteLine shortcut.
        /// </summary>
        /// <param name="a">String to be written.</param>
        public static void Print(string a)
        {
            Console.WriteLine(a);
        }

        public static void SetCursorPos(int left, int down)
        {
            Console.SetCursorPosition(left, down);
        }

        public static void Wait()
        {
            Console.ReadKey();
        }

        /// <summary>
        /// Console.Write shortcut.
        /// </summary>
        /// <param name="a">String to be written.</param>
        public static void Write(string a)
        {
            Console.Write(a);
        }

        public static string yN(string s) => s + "\n\nY/N\n";

        #endregion Code Shortcuts
    }
}
