﻿using LagunaConsoleCollection.Games.AdventureGames.MapGen.Modules;
using LagunaConsoleCollection.Games.AdventureGames.MapGen.Objects;
using System;

namespace LagunaConsoleCollection.Games.AdventureGames.MapGen
{
    internal class Game
    {
        #region Private Fields

        const int ACTION_HEIGHT = 50;
        const int ACTION_WIDTH = GAME_WIDTH;
        const ConsoleColor BACKGROUND_COLOR = ConsoleColor.Black;
        const int GAME_HEIGHT = 57;
        const int GAME_WIDTH = 128;
        const ConsoleColor HERO_COLOR = ConsoleColor.White;
        static Player Hero;
        static bool isRunning;
        static World world;

        #endregion Private Fields

        #region Public Properties

        public static bool IsRunning
        {
            get => isRunning;
            set => isRunning = value;
        }

        internal static Buildings[] Townandcastle { get; set; }

        #endregion Public Properties

        #region Public Constructors

        public Game()
        {
            IsRunning = false;
            Init();
        }



        #endregion Public Constructors

        #region Public Methods

        public static void Start()
        {
            isRunning = true;
            Init();

            while (isRunning)
            {
                Update();
                Draw();
            }
        }

        static void Draw()
        {
            //Console.BackgroundColor = ConsoleColor.Cyan;
            //Shortcuts.SetCursorPos(Hero.coords.X, Hero.coords.Y);
            //Console.Write("L");
        }

        static void Update()
        {
            //String commandLineInput = Console.ReadLine();
            OnMessage(TakeInput());
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Make sure that the new coordinate is not placed outside the
        /// console window (since that will cause a runtime crash
        /// </summary>
        static bool CanMove(
            Player p)
        {
            Coordinate c = p.mapCoords;
            if (c.X < 0
                || c.X >= ACTION_WIDTH)
                return false;

            if (c.Y < 0
                || c.Y >= ACTION_HEIGHT)
                return false;

            if (p.standingOn(world.Grid).Symbol == 'W')
                return false;

            return true;
        }

        static void Init()
        {
            // Initialize the random number generator.

            if (OperatingSystem.IsWindows())
            {
                Console.SetWindowSize(GAME_WIDTH, GAME_HEIGHT + 1);
            }
            SetBackgroundColor();
            Hero = new Player();
            world = new World(ACTION_WIDTH, ACTION_HEIGHT);
            Townandcastle = new Buildings[101];
            //world.generateWorld(25, 25);
            world.fillworld(42, 0, 0, 0, 0);
            world.generateworld2(3, 6, 6, 65, 10, 10);
            Random ran = new Random(); // Hero starts on non-water not at the edge
            int startposy = ran.Next(5, GAME_WIDTH - 5);
            int startposx = ran.Next(5, GAME_HEIGHT - 5);
            while (world.Grid[startposy][startposx].ID == 0 && world.Grid[startposy][startposx].BuildingID == 0)
            {
                startposx = ran.Next(5, GAME_HEIGHT - 5);
                startposy = ran.Next(5, GAME_WIDTH - 5);
            }
            world.placehome(startposy, startposx);
            int buildingindex = 0;
            for (int i = 1; i < ACTION_WIDTH - 1; i++)
            {
                for (int j = 2; j < ACTION_HEIGHT - 2; j++)
                {
                    if (world.Grid[i][j].BuildingID > 0)
                    {
                        Townandcastle[buildingindex] = new Buildings
                        {
                            ID = world.Grid[i][j].BuildingID,
                            Name = world.Grid[i][j].Name
                        };
                        Townandcastle[buildingindex].MapCoord.X = j;
                        Townandcastle[buildingindex].MapCoord.Y = i;
                        world.Grid[i][j].BuildingExtra = buildingindex;
                        buildingindex++;
                    }
                }
            }
            Townandcastle[100] = new Buildings
            {
                ID = 1
            };
            Townandcastle[100].MapCoord.X = startposx;
            Townandcastle[100].MapCoord.Y = startposy;
            Townandcastle[100].Name = "Home";
            world.draw();
            MoveHero(startposy, startposx);
            isRunning = true;
            Console.Title = $"{world.Name} - MapGenTest";
        }

        /// <summary>
        /// Paint the new hero
        /// </summary>
        static void MoveHero(
            int x,
            int y)
        {
            Player oldHero = Hero;
            Player newHero = new Player();
            newHero.mapCoords.X = oldHero.mapCoords.X + x;
            newHero.mapCoords.Y = oldHero.mapCoords.Y + y;

            if (CanMove(newHero))
            {
                RemoveHero();

                Console.BackgroundColor = HERO_COLOR;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition(
                    newHero.mapCoords.X,
                    newHero.mapCoords.Y
                    );
                Console.Write("@");
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = BACKGROUND_COLOR;
                Console.SetCursorPosition(0, GAME_HEIGHT - 3);

                // This writes the text at the bottom of the map screen.
                Console.Write(world.Grid[newHero.mapCoords.X][newHero.mapCoords.Y].Name + " "
                    //+ world.Grid[newHero.mapCoords.X][newHero.mapCoords.Y].IDExtra 
                    + new string(' ', Console.WindowWidth - 1));

                Hero.mapCoords = newHero.mapCoords;
                //Console.Beep();
                if (newHero.mapCoords.X != oldHero.mapCoords.X
                    || newHero.mapCoords.Y != oldHero.mapCoords.Y)
                {
                    Console.Beep();
                }
            }
        }

        /// <summary>
        /// Over-paint the old hero
        /// </summary>
        static void RemoveHero()
        {
            int x = Hero.mapCoords.X;
            int y = Hero.mapCoords.Y;
            Console.SetCursorPosition(x, y);
            if (world.Grid[x][y].BuildingID == 2)
            {
                world.drawSymbol(ConsoleColor.Magenta, ConsoleColor.DarkRed, world.Grid, x, y);
            }
            else if (world.Grid[x][y].BuildingID == 3)
            {
                world.drawSymbol(ConsoleColor.Black, ConsoleColor.Magenta, world.Grid, x, y);
            }
            else if (world.Grid[x][y].BuildingID == 1)
            {
                world.drawSymbol(ConsoleColor.White, ConsoleColor.Black, world.Grid, x, y);
            }
            else if (world.Grid[x][y].Symbol == 'W')
            {
                world.drawSymbol(ConsoleColor.DarkCyan, ConsoleColor.Cyan, world.Grid, x, y);
            }
            else if (world.Grid[x][y].Symbol == '&')
            {
                world.drawSymbol(ConsoleColor.Green, ConsoleColor.DarkGreen, world.Grid, x, y);
            }
            else if (world.Grid[x][y].Symbol == 'V')
            {
                world.drawSymbol(ConsoleColor.Black, ConsoleColor.DarkGray, world.Grid, x, y);
            }
            else if (world.Grid[x][y].Symbol == '#')
            {
                world.drawSymbol(ConsoleColor.DarkGreen, ConsoleColor.Black, world.Grid, x, y);
            }
        }

        /// <summary>
        /// Paint a background color
        /// </summary>
        /// <remarks>
        /// It is very important that you run the Clear() method after
        /// changing the background color since this causes a repaint of the background
        /// </remarks>
        static void SetBackgroundColor()
        {
            Console.BackgroundColor = BACKGROUND_COLOR;
            Console.Clear(); //Important!
        }

        static String TakeInput()
        {
            Console.BackgroundColor = BACKGROUND_COLOR;
            Shortcuts.SetCursorPos(
                0,
                GAME_HEIGHT-1
                );
            String input = Console.ReadLine();
            Console.SetCursorPosition(0, 0);
            world.draw();
            //        MoveHero(0, 0);
            return input;
        }

        /// <summary>
        /// Handles user input to the command line. Runs during Game.Update().
        /// </summary>
        /// <param name="msg">The users line input.</param>
        static void OnMessage(string msg)
        {
            int msgL = msg.Length;
            if (!(msgL <= 0))
            {
                String[] cmd = msg.Split(
                    new Char[] { ' ' });
                if (msg.Substring(0, 1) == "/")
                {
                    if (cmd[0].ToLower() == "/map")
                    {
                        if (cmd[1].ToLower() == "regen")
                        {
                            Console.Clear();
                            world = new World(ACTION_WIDTH, ACTION_HEIGHT);
                            Townandcastle = new Buildings[101];
                            world.fillworld(42, 0, 0, 0, 0);
                            world.generateworld2(3, 5, 7, 65, 10, 10);
                            Random ran = new Random(); // Hero starts on non-water not at the edge
                            int startposx = ran.Next(5, ACTION_HEIGHT - 5);
                            int startposy = ran.Next(5, ACTION_WIDTH - 5);
                            while (world.Grid[startposy][startposx].ID == 0 && world.Grid[startposy][startposx].BuildingID == 0)
                            {
                                startposx = ran.Next(5, GAME_HEIGHT - 5);
                                startposy = ran.Next(5, GAME_WIDTH - 5);
                            }
                            int buildingindex = 0;
                            for (int i = 1; i < ACTION_WIDTH - 1; i++)
                            {
                                for (int j = 2; j < ACTION_HEIGHT - 2; j++)
                                {
                                    if (world.Grid[i][j].BuildingID > 0)
                                    {
                                        Townandcastle[buildingindex] = new Buildings
                                        {
                                            ID = world.Grid[i][j].BuildingID,
                                            Name = world.Grid[i][j].Name
                                        };
                                        Townandcastle[buildingindex].MapCoord.X = j;
                                        Townandcastle[buildingindex].MapCoord.Y = i;
                                        world.Grid[i][j].BuildingExtra = buildingindex;
                                        buildingindex++;
                                    }
                                }
                            }
                            world.placehome(startposy, startposx);
                            Townandcastle[100] = new Buildings
                            {
                                ID = 1
                            };
                            Townandcastle[100].MapCoord.X = startposx;
                            Townandcastle[100].MapCoord.Y = startposy;
                            Townandcastle[100].Name = "Home";
                            world.draw();
                            MoveHero(startposx, startposy);
                        }
                    }
                }
                else
                {
                    if (msg.ToLower() == "fuck all this, get me out of here now!")
                    {
                        isRunning = false;
                    }
                    if (cmd[0].ToLower() == "move" ||
                        cmd[0].ToLower() == "mov" ||
                        cmd[0].ToLower() == "mo" ||
                        cmd[0].ToLower() == "m" ||
                        cmd[0].ToLower() == "u" ||
                        cmd[0].ToLower() == "d" ||
                        cmd[0].ToLower() == "l" ||
                        cmd[0].ToLower() == "r")
                    {
                        foreach (String str in cmd)
                        {
                            if (str.ToLower() == "up" ||
                                str.ToLower() == "u")
                            {
                                MoveHero(0, -1);
                            }
                            if (str.ToLower() == "down" ||
                                str.ToLower() == "dow" ||
                                str.ToLower() == "do" ||
                                str.ToLower() == "d")
                            {
                                MoveHero(0, 1);
                            }
                            if (str.ToLower() == "left" ||
                                str.ToLower() == "lef" ||
                                str.ToLower() == "le" ||
                                str.ToLower() == "l")
                            {
                                MoveHero(-1, 0);
                            }
                            if (str.ToLower() == "right" ||
                                str.ToLower() == "righ" ||
                                str.ToLower() == "rig" ||
                                str.ToLower() == "ri" ||
                                str.ToLower() == "r")
                            {
                                MoveHero(1, 0);
                            }
                        }
                    }
                    Console.BackgroundColor = HERO_COLOR;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.SetCursorPosition(
                        Hero.mapCoords.X,
                        Hero.mapCoords.Y
                        );
                    Console.Write("@");
                    Console.BackgroundColor = BACKGROUND_COLOR;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.SetCursorPosition(0, GAME_HEIGHT);
                    Console.Write(new string(' ', Console.WindowWidth - 1));
                }
            }
        }

        #endregion Private Methods
    }
}
