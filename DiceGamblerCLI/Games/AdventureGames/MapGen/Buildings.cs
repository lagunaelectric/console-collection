﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LagunaConsoleCollection.Games.AdventureGames.MapGen
{
    internal class Buildings
    {
        /// <summary>
        /// 1:home, 2:castle, 3 town
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// its position on the map
        /// </summary>
        public Modules.Coordinate MapCoord { get; set; } = new Modules.Coordinate(); //

        /// <summary>
        /// I do like the seaside...
        /// </summary>
        public bool IsCoastal { get; set; }
    }
}