﻿//#define MG_DEBUG
#define PETAL_DEBUG
//#define DICE_DEBUG

using static System.Console;
using System;
using ConsoleGameHelper;

namespace LagunaConsoleCollection
{



    /// <summary>
    /// This acts as the main game screen and gives the user a way to choose which game
    /// he/she would like to play.
    /// </summary>
    class GameSelector
    {
        //const string PRESS_CONTINUE = "Press the any key...";
        //static readonly string n = Environment.NewLine;

        static readonly TextHelper th = new TextHelper();

        /// <summary>
        /// We need this to handle the users input in a simpler way than a huge if/else if/ else
        /// statement.
        /// </summary>
        static readonly Commands commands = new Commands();

        public void Start()
        {
#if(MG_DEBUG)
            var input = commands.ProcessCommand("mapgen");
#elif (PETAL_DEBUG)
            var input = commands.ProcessCommand("petals");
#elif(DICE_DEBUG)
            var input = commands.ProcessCommand("dice2");
#elif (!MG_DEBUG && !PETAL_DEBUG && !DICE_DEBUG)
            Clear();
            WriteLine("Welcome to the game selector!");
            WriteLine(th.n);
            Write(th.PRESS_CONTINUE);
            ReadKey();
            Clear();
            Write("Type A for B:"
                + th.n + "> dice2    | Dice Gambler - 2 Dice"
                + th.n + "> petals   | Petals Around the Rose"
                + th.n + "> mapgen   | Map Generator R&D"
                );
            WriteLine();
            Write(th.n + th.n + "> ");
            var input = commands.ProcessCommand(ReadLine());
            if (input != null)
                WriteLine(input);
            ReadKey();
            Start();
#endif
        }
    }
}