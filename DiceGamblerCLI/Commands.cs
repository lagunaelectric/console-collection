﻿using ConsoleGameHelper;
using static System.Console;


namespace LagunaConsoleCollection
{
    class Commands : CommandProcessor
    {
        public bool ExitRequested { get; set; }

        public Commands()
        {
            ExitRequested = false;
        }

        public void Dice2()
        {
            Title = "Loading Dice Gamble...";
            Games.DiceGames.TwoDiceGambler.Game.Start();
        }

        public void Petals()
        {
            Title = "Loading Petals Around the Rose...";
            Games.DiceGames.PetalsAroundTheRose.Game.Start();
        }

        public void MapGen()
        {
            Title = "Loading the universe...";
            Games.AdventureGames.MapGen.Game.Start();
        }
    }
}