﻿using System;

namespace LagunaConsoleCollection
{
    class Program
    {
        static void Main()
        {
            if (OperatingSystem.IsWindows())
            {
                Console.BufferHeight = Console.WindowHeight;
                Console.BufferWidth = Console.WindowWidth;

            }
            var selector = new GameSelector();
            selector.Start();
        }
    }
}