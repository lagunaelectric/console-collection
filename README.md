# Console Collection
This is a collection of console games written for the command line.

## Console Game Helper
Console Game Helper is a class library I wrote to hold common classes and functions used throughout this project. It can be used standalone in any other project as well.

## Dice Gambler CLI
This is a very poorly named folder that holds the games for this project