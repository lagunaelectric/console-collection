﻿using System;

namespace ConsoleGameHelper
{
    public class Terrain
    {
        const string VOID_NAME = "";
        const string VOID_DESC = "Void.";
        const ConsoleColor VOID_COLOR = ConsoleColor.Black;
        const string GRASS_NAME = "Grass";
        const string GRASS_DESC = "Soft green grass that blows gently in the breeze.";
        const ConsoleColor GRASS_COLOR = ConsoleColor.Green;
        const string DIRT_NAME = "Dirt";
        const string DIRT_DESC = "Bare ground with rocks.";
        const ConsoleColor DIRT_COLOR = ConsoleColor.DarkMagenta;
        public string Name => name;
        public string Description => desc;
        public ConsoleColor Color => color;
        static string name;
        static string desc;
        static ConsoleColor color;

        public Terrain()
        {
            name = "";
            desc = "";
            color = ConsoleColor.Black;
        }

        public Terrain(TerrainType type)
        {
            SetTerrain(type);
        }

        Terrain(string aName, string aDesc, ConsoleColor aColor)
        {
            name = aName;
            desc = aDesc;
            color = aColor;
        }

        public void SetTerrain(TerrainType type)
        {
            switch (type)
            {
                case TerrainType.Grass:
                    name = GRASS_NAME;
                    desc = GRASS_DESC;
                    color = GRASS_COLOR;
                    break;

                case TerrainType.Dirt:
                    name = DIRT_NAME;
                    desc = DIRT_DESC;
                    color = DIRT_COLOR;
                    break;

                case TerrainType.Void:
                    name = VOID_NAME;
                    desc = VOID_DESC;
                    color = VOID_COLOR;
                    break;
            }
        }
    }
}