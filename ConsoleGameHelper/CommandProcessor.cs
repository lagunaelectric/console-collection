﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using static System.Console;

namespace ConsoleGameHelper
{
    /// <summary>
    /// Processes a string to activate methods via reflection.
    /// </summary>
    /// <remarks>
    /// This class was written by Anthony Stivers.
    ///   Thanks man! I was introduced to reflection
    ///   in this class :) -- LagunaE
    /// </remarks>
    public abstract class CommandProcessor
    {
        public string ProcessCommand(string commandWithParameters)
        {
            try
            {
                string command = commandWithParameters.Split(' ')[0];
                string[] commandParameters = commandWithParameters.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Skip(1).ToArray();
                MethodInfo method = GetType().GetMethod(command, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                ParameterInfo[] methodParameters = method?.GetParameters();

                if (methodParameters?.Length != commandParameters?.Length)
                    throw new Exception("Invalid parameter count.");

                var parametersToSend = new List<object>();
                for (int i = 0; i < methodParameters.Length; i++)
                {
                    Type parameterType = methodParameters[i].ParameterType;
                    object parameterValue = TypeDescriptor.GetConverter(parameterType).ConvertFromInvariantString(commandParameters[i]);
                    parametersToSend.Add(parameterValue);
                }
                method.Invoke(this, parametersToSend.ToArray());
            }
            catch (Exception ex)
            {
                Clear();
                return ex.Message;
            }

            return null;
        }
    }
}