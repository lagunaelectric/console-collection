﻿using System;

namespace ConsoleGameHelper
{
    interface IColorable
    {
        void SetConsoleColors();
    }
}