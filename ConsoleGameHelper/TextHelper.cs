﻿using System;

namespace ConsoleGameHelper
{
    /// <summary>
    /// A little extra hand for all of those commonly used strings and a newline to boot!
    /// </summary>
    public class TextHelper
    {
        public readonly string PRESS_CONTINUE = "Press the any key...";

        /// <summary>
        /// A short and sweet representation of Environment.NewLine to help your code look neater.
        /// </summary>
        public readonly string n = Environment.NewLine;
    }
}