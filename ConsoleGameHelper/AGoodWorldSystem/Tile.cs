﻿using System;
using static System.Console;

namespace ConsoleGameHelper.AGoodWorldSystem
{
    public class Tile : IColorable
    {
        public Tile()
        {
            ID = 0;
            IDExtra = 0;
            Difficulty = 1;
            BuildingID = 0;
            BuildingExtra = 0;
            Symbol = '-';
        }

        public int ID { get; set; }

        /// <summary>
        /// Tile symbol
        /// </summary>
        public char Symbol { get; set; }

        /// <summary>
        /// 0:none, 1:home, 2:castle, 3:town
        /// </summary>
        public int BuildingID { get; set; }

        /// <summary>
        /// Grows as tiles move from Home
        /// </summary>
        public int Difficulty { get; set; }

        /// <summary>
        /// 0:water, 1:grasslands, 2:mountain, 3:forest
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// additional information for the building
        /// </summary>
        public int BuildingExtra { get; set; }

        /// <summary>
        /// additional information for the tile
        /// </summary>
        public int IDExtra { get; set; }

        /// <summary>
        /// Sets the console colors to those appropriate for the tiles ID/Symbol.
        /// </summary>
        public void SetConsoleColors()
        {
            if (BuildingID == 2)
            {
                ForegroundColor = ConsoleColor.Magenta;
                BackgroundColor = ConsoleColor.DarkRed;
            }
            else if (BuildingID == 3)
            {
                ForegroundColor = ConsoleColor.Black;
                BackgroundColor = ConsoleColor.Magenta;
            }
            else if (Symbol == 'W')
            {
                ForegroundColor = ConsoleColor.DarkCyan;
                BackgroundColor = ConsoleColor.Cyan;
            }
            else if (Symbol == '&')
            {
                ForegroundColor = ConsoleColor.Green;
                BackgroundColor = ConsoleColor.DarkGreen;
            }
            else if (Symbol == 'V')
            {
                ForegroundColor = ConsoleColor.Black;
                BackgroundColor = ConsoleColor.DarkGray;
            }
            else if (Symbol == '#')
            {
                ForegroundColor = ConsoleColor.DarkGreen;
                BackgroundColor = ConsoleColor.Black;
            }
        }
    }
}