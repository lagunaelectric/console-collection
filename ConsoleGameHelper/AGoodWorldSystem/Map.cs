﻿using System;
using System.Collections.Generic;

namespace ConsoleGameHelper.AGoodWorldSystem
{
    public class Map : List<TileList>, IColorable
    {
        /// <summary>
        /// Map symbol
        /// </summary>
        public char Symbol { get; set; }

        public int ID { get; set; }

        /// <summary>
        /// Some foresight into possibly wanting every map to
        /// be uniquely identified with zero duplicate ID's.
        /// </summary>
        public Guid GUID { get; } = Guid.NewGuid();

        public void SetConsoleColors()
        {
            Console.BackgroundColor = ConsoleColor.Green;
            Console.ForegroundColor = ConsoleColor.Cyan;
        }
    }
}