﻿namespace ConsoleGameHelper
{
    public enum TerrainType
    {
        Void = 0,
        Grass = 1,
        Dirt = 2
    }
}