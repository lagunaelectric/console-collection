﻿using System;

namespace ConsoleGameHelper
{
    public class Dice
    {
        /// <summary>
        /// The face value of the Dice.
        /// </summary>
        public int FaceValue { get; set; } = 1;

        /// <summary>
        /// Only a Potentate of the Rose should know what this is for...
        /// </summary>
        public int RosePetals => (FaceValue == 3 || FaceValue == 5) ? FaceValue - 1 : 0;

        public static implicit operator int(Dice dice) => dice.FaceValue;

        public static implicit operator Dice(int number) => new Dice { FaceValue = number };
    }
}